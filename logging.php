<?php
//
// http://localhost/api/logging.php
//
function getUserIP()
{
    // Get real visitor IP behind CloudFlare network
    if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
              $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
              $_SERVER['HTTP_CLIENT_IP'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
    }
    $client  = @$_SERVER['HTTP_CLIENT_IP'];
    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
    $remote  = $_SERVER['REMOTE_ADDR'];

    if(filter_var($client, FILTER_VALIDATE_IP))
    {
        $ip = $client;
    }
    elseif(filter_var($forward, FILTER_VALIDATE_IP))
    {
        $ip = $forward;
    }
    else
    {
        $ip = $remote;
    }

    return $ip;
}
/*
logUserIP();
$arr["request_headers"] = apache_request_headers();
$arr["method"] = $_SERVER['REQUEST_METHOD'];

//$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"
$url = $_SERVER['REQUEST_URI'];
$query_str = parse_url($url, PHP_URL_QUERY);
parse_str($query_str, $query_params);
//print_r($query_params);
if (!empty($query_params)){
	$arr["params"] = $query_params;
}
if(!$_SERVER['SERVER_ADDR']){ $_SERVER['SERVER_ADDR'] = $_SERVER['LOCAL_ADDR']; }
$arr["serverIP"] = $_SERVER['SERVER_ADDR'];
$arr["remoteIP"] = getUserIP();
echo json_encode($arr);
*/
function getDb(){
	//local
	$mysql_host     = "127.0.0.1";
	$mysql_username = "root";
	$mysql_password = "";
	$mysql_database = "test";
	
	//00webhost
	$mysql_host     = "localhost";
	$mysql_username = "id8455429_dbuser";
	$mysql_password = "dbpassword";
	$mysql_database = "id8455429_dbone";


	$link = mysqli_connect($mysql_host, $mysql_username, $mysql_password, $mysql_database);

	if (!$link) {
		echo "Error: Unable to connect to MySQL." . PHP_EOL;
		echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
		echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
		return;
	} else {
		return $link;
	}
}


function logUserIPfull($param,$headers){
	
	$user_ip = getUserIP();

	$link = getDb();
	
	$p = json_encode($param);
	
	$h = json_encode($headers);

	$sql = "INSERT INTO log (ip,param,headers) VALUES ('".($user_ip)."','".$p."','".$h."')".PHP_EOL;

	//echo $sql;

	if (mysqli_query($link, $sql)) {
		//echo "{result:true}".PHP_EOL;
	} else {
		echo "Error: " . $sql . "<br>" . mysqli_error($link).PHP_EOL;
	}
	
	mysqli_close($link);
}

function logUserIP()
{
	$user_ip = getUserIP();

	$link = getDb();

	$sql = "INSERT INTO log (ip) VALUES ('".($user_ip)."')".PHP_EOL;

	//echo $sql;

	if (mysqli_query($link, $sql)) {
		//echo "New record created successfully".PHP_EOL;
	} else {
		//echo "Error: " . $sql . "<br>" . mysqli_error($link).PHP_EOL;
	}
	
	mysqli_close($link);
}
//INSERT INTO `log`(`ip`) VALUES ('0.0.0.2') 
?>